# -*- coding: utf-8 -*-

"""Module init code."""

__version__ = "0.1.0"

__author__ = "Matteo Di Carlo"
__email__ = "matteo.dicarlo@inaf.it"
