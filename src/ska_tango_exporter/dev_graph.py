import json
import logging
import os
import random

from flask import Flask, jsonify, request
from influxdb_client import InfluxDBClient
from kubernetes import client, config

app = Flask(__name__)

NAMESPACE = os.getenv("NAMESPACE", "default")
config.load_incluster_config()

LOG_LEVEL = os.getenv("LOG_LEVEL", "INFO")
level = logging.getLevelName(LOG_LEVEL)
logging.basicConfig(level=level)


class_colors = {}

influx_url = os.environ.get("INFLUXDB_URL")
influx_bucket_name = os.environ.get("INFLUXDB_BUCKET_NAME", "tango-events")
influx_db_token = os.environ.get("INFLUX_DB_TOKEN")
influx_db_org = os.environ.get("INFLUX_DB_ORG")


def get_number_of_events(dev):
    try:
        with InfluxDBClient(
            url=influx_url,
            token=influx_db_token,
            org=influx_db_org,
            debug=False,
        ) as influx_client:
            query_api = influx_client.query_api()
            query_text = [
                f'from(bucket: "{influx_bucket_name}")',
                "|> range(start: -500m)",
                f'|> filter(fn: (r) => r["_measurement"] == "{dev}")',
                '|> filter(fn: (r) => r["_field"] == "attr_value")',
                "|> count()",
                '|> yield(name: "count")',
            ]
            tables = query_api.query("".join(query_text))
            result = 0
            for table in tables:
                logging.debug("Processing table %s", table)
                for record in table.records:
                    logging.debug("Processing record %s", record)
                    logging.debug("get_value %s", record.get_value())
                    result += record.get_value()
                    logging.debug("result %s", result)
            return result
    except Exception as e:
        logging.error("InfluxDBClient error: %s", str(e), stack_info=True)
        return 0


@app.route("/api/graph/fields")
def fetch_graph_fields():
    nodes_fields = [
        {"field_name": "id", "type": "string"},
        {
            "field_name": "title",
            "type": "string",
        },
        {"field_name": "subtitle", "type": "string"},
        {
            "field_name": "mainstat",
            "type": "string",
            "displayName": "Events pushed",
            "color": "green",
        },
        {
            "field_name": "secondarystat",
            "type": "string",
            "displayName": "Polled Attribute",
            "color": "red",
        },
        {"field_name": "color", "type": "string"},
        # {
        #     "field_name": "arc__events",
        #     "type": "number",
        #     "color": "green",
        # },
        # {
        #     "field_name": "arc__polled",
        #     "type": "number",
        #     "color": "red",
        # },
        {
            "field_name": "detail__state",
            "type": "string",
            "displayName": "DS State",
        },
    ]
    edges_fields = [
        {"field_name": "id", "type": "string"},
        {"field_name": "source", "type": "string"},
        {"field_name": "target", "type": "string"},
        # {"field_name": "mainstat", "type": "number"},
    ]
    result = {"nodes_fields": nodes_fields, "edges_fields": edges_fields}
    return jsonify(result)


@app.route("/api/graph/data")
def fetch_graph_data():
    domain = request.args.get("domain", "")
    family = request.args.get("family", "")
    member = request.args.get("member", "")

    custom_object_api = client.CustomObjectsApi()
    deviceservers = custom_object_api.list_namespaced_custom_object(
        group="tango.tango-controls.org",
        version="v1",
        namespace=NAMESPACE,
        plural="deviceservers",
    )

    nodes = []
    edges = []
    devices = []
    edge_id = 1
    source_targets = []

    # add databaseds device
    node = {
        "id": "sys/database/2",
        "title": "sys/database/2",
        "subtitle": "DatabaseDS",
        "detail__state": "Running",
        "mainstat": 0,
        "secondarystat": 0,
        "color": "gray",
    }
    nodes.append(node)

    for item in deviceservers["items"]:
        logging.debug("Working on item: %s", item)
        dsconfig_json = item["spec"]["config"]
        dsconfig = json.loads(dsconfig_json)
        for server in dsconfig["servers"]:
            for instance in dsconfig["servers"][server]:
                for cls in dsconfig["servers"][server][instance]:
                    if cls not in class_colors:
                        class_colors[cls] = "#" + "".join(
                            [
                                random.choice("0123456789ABCDEF")
                                for j in range(6)
                            ]
                        )
                    for dev in dsconfig["servers"][server][instance][cls]:
                        if (
                            domain is not None
                            and domain != ""
                            and domain not in dev
                        ):
                            continue
                        if (
                            member is not None
                            and member != ""
                            and member not in dev
                        ):
                            continue
                        if (
                            family is not None
                            and family != ""
                            and family not in dev
                        ):
                            continue

                        if dev not in devices:
                            polled_attr = 0
                            if (
                                "properties"
                                in dsconfig["servers"][server][instance][cls][
                                    dev
                                ]
                            ):
                                for prop in dsconfig["servers"][server][
                                    instance
                                ][cls][dev]["properties"]:
                                    if prop == "polled_attr":
                                        polled_attr += 1
                            polled_str = str(polled_attr)
                            event_num = get_number_of_events(dev)
                            node = {
                                "id": dev,
                                "title": dev,
                                "subtitle": f"{server}/{instance} (Device class: {cls})",
                                "detail__state": item["status"]["state"],
                                "mainstat": str(event_num),
                                "secondarystat": polled_str,
                                "color": class_colors[cls],
                            }
                            devices.append(dev)
                            nodes.append(node)
        if "dependsOn" in item["spec"]:
            dependsOn_items = item["spec"]["dependsOn"]
            for dependsOn in dependsOn_items:
                if "/" in dependsOn:
                    for dev in devices:
                        source_target = f"{dev}-{dependsOn}"
                        if source_target not in source_targets:
                            edge = {
                                "id": edge_id,
                                "source": dev,
                                "target": dependsOn,
                            }
                            edges.append(edge)
                            source_targets.append(source_target)
                            edge_id = edge_id + 1

    result = {"nodes": nodes, "edges": edges}
    return jsonify(result)


@app.route("/api/health")
def check_health():
    return "API is working well!"


app.run(host="0.0.0.0", port=5000)
