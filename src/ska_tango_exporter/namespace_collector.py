import argparse
import logging
import time

from prometheus_client import start_http_server
from prometheus_client.core import REGISTRY
from tango import AttrDataFormat, Database, DevFailed, DeviceProxy

from ska_tango_exporter.model import Metrics
from ska_tango_exporter.utils import build_fqdn, databaseds_name


class NamespaceCollector(object):
    def __init__(self):
        self.total = None
        self.tango_host = None
        self.namespace = None
        self.replicas = None
        self.replica_id = None

    def collect(self):
        metrics = Metrics()
        total_count = 0
        scalar_count = 0
        error_count = 0
        error_attr_count = 0
        scalar_count = 0
        spectrum_count = 0
        image_count = 0
        not_managed_attribute_count = 0
        start = time.time()
        logging.debug("start namespace collector %s ", str(self.replica_id))
        logging.debug(
            "connecting to %s",
            build_fqdn(self.tango_host, self.namespace, databaseds_name),
        )
        server_list = []
        try:
            database = Database()
            server_list = database.get_server_list().value_string
        except Exception as ex:
            logging.error(ex)
            metrics.add_totals()
            return (
                metrics.attribute_metrics,
                metrics.errors,
                metrics.errors_attr,
                metrics.attribute_read_count,
                metrics.attribute_count,
                metrics.spectrum_attribute_count,
                metrics.image_attribute_count,
                metrics.not_managed,
                metrics.namespaces_read_time,
                metrics.thread_read_time,
            )

        logging.debug("server list= %s", str(len(server_list)))
        count = int(len(server_list) / self.replicas)
        i = int(count * self.replica_id)
        count += i
        if self.replicas - 1 == self.replica_id:
            count = len(server_list)

        logging.info(
            "Namespace=%s, ReplicaID=%s, i=%s, count=%s, len(server_list)=%s",
            self.namespace,
            str(self.replica_id),
            str(i),
            str(count),
            str(len(server_list)),
        )

        while i < count and count <= len(server_list):
            # https://pytango.readthedocs.io/en/stable/database.html#tango.Database.get_device_class_list
            logging.debug("get classlist from %s", str(server_list[i]))
            class_list = database.get_device_class_list(
                str(server_list[i])
            ).value_string
            logging.debug("len(class_list)= %s", str(len(class_list)))
            j = 0
            while j < len(class_list):
                try:
                    if "dserver" in class_list[j]:
                        logging.debug("skipping %s", class_list[j])
                        j += 2
                        continue
                    if "/" not in class_list[j]:
                        logging.debug("skipping %s", class_list[j])
                        j += 2
                        continue
                    logging.debug(
                        build_fqdn(
                            self.tango_host, self.namespace, class_list[j]
                        )
                    )
                    dev = DeviceProxy(
                        build_fqdn(
                            self.tango_host, self.namespace, class_list[j]
                        )
                    )

                    dev.set_timeout_millis(10)
                    attr_list = dev.attribute_list_query()
                    for attr_info in attr_list:
                        try:
                            logging.debug("       %s", attr_info.name)
                            total_count += 1
                            # 1: tango._tango.CmdArgType.DevBoolean,
                            # 2: tango._tango.CmdArgType.DevShort,
                            # 3: tango._tango.CmdArgType.DevLong,
                            # 4: tango._tango.CmdArgType.DevFloat,
                            # 5: tango._tango.CmdArgType.DevDouble,
                            # 6: tango._tango.CmdArgType.DevUShort,
                            # 7: tango._tango.CmdArgType.DevULong,
                            # 8: tango._tango.CmdArgType.DevString,
                            # 19: tango._tango.CmdArgType.DevState,
                            # 23: tango._tango.CmdArgType.DevLong64,
                            # 24: tango._tango.CmdArgType.DevULong64,
                            # 27: tango._tango.CmdArgType.DevInt,
                            # 29: tango._tango.CmdArgType.DevEnum,
                            if attr_info.data_format == AttrDataFormat.SCALAR:
                                res = metrics.add_to_metric(
                                    self.namespace, dev, attr_info
                                )
                                if res > 0:
                                    scalar_count = scalar_count + res
                                    scalar_count += 1
                                else:
                                    #  0: tango._tango.CmdArgType.DevVoid,
                                    #  28: tango._tango.CmdArgType.DevEncoded,
                                    #  30: tango._tango.CmdArgType.DevPipeBlob,
                                    #  22: tango._tango.CmdArgType.DevUChar,
                                    #  20: tango._tango.CmdArgType.ConstDevString,
                                    not_managed_attribute_count += 1
                                    logging.debug(
                                        "*******NOT MANAGED: %s",
                                        attr_info.name,
                                    )
                            #  9: tango._tango.CmdArgType.DevVarCharArray,
                            #  10: tango._tango.CmdArgType.DevVarShortArray,
                            #  11: tango._tango.CmdArgType.DevVarLongArray,
                            #  12: tango._tango.CmdArgType.DevVarFloatArray,
                            #  13: tango._tango.CmdArgType.DevVarDoubleArray,
                            #  14: tango._tango.CmdArgType.DevVarUShortArray,
                            #  15: tango._tango.CmdArgType.DevVarULongArray,
                            #  16: tango._tango.CmdArgType.DevVarStringArray,
                            #  17: tango._tango.CmdArgType.DevVarLongStringArray,
                            #  18: tango._tango.CmdArgType.DevVarDoubleStringArray,
                            #  21: tango._tango.CmdArgType.DevVarBooleanArray,
                            #  25: tango._tango.CmdArgType.DevVarLong64Array,
                            #  26: tango._tango.CmdArgType.DevVarULong64Array,
                            #  31: tango._tango.CmdArgType.DevVarStateArray}
                            elif (
                                attr_info.data_format
                                == AttrDataFormat.SPECTRUM
                            ):
                                res = metrics.add_to_metric_spectrum(
                                    self.namespace, dev, attr_info
                                )
                                if res <= 0:
                                    not_managed_attribute_count += 1
                                    logging.debug(
                                        "*******NOT MANAGED: %s",
                                        attr_info.name,
                                    )
                                else:
                                    spectrum_count += 1
                                    scalar_count += 1

                            elif attr_info.data_format == AttrDataFormat.IMAGE:
                                # res = metrics.add_to_metric_image(dev, attr_info, attribute_metrics)
                                # if(res <= 0):
                                not_managed_attribute_count += 1
                                logging.debug(
                                    "*******NOT MANAGED: %s", attr_info.name
                                )
                                image_count += 1
                                # read_count += 1
                            else:
                                # AttrDataFormat.FMT_UNKNOWN
                                not_managed_attribute_count += 1
                                logging.debug(
                                    "*******NOT MANAGED: %s", attr_info.name
                                )
                        except DevFailed as df:
                            logging.error(
                                "Could not connect to the '%s.%s'. Namespace=%s, ReplicaID=%s, Reason=%s",
                                class_list[j],
                                attr_info.name,
                                self.namespace,
                                str(self.replica_id),
                                df.args[0].reason,
                            )
                            # logging.error(e1)
                            error_attr_count += 1
                except DevFailed as df:
                    logging.error(
                        "Could not connect to the '%s' DeviceProxy. Namespace=%s, ReplicaID=%s, Reason=%s",
                        class_list[j],
                        self.namespace,
                        str(self.replica_id),
                        df.args[0].reason,
                    )
                    error_count += 1
                j += 2
            i += 1
        metrics.sum_totals(
            self.namespace,
            error_count,
            error_attr_count,
            total_count,
            scalar_count,
            spectrum_count,
            image_count,
            not_managed_attribute_count,
        )
        self.total = time.time() - start
        logging.info(
            "Replica %s has taken %s ss for reading namespace %s",
            str(self.replica_id),
            str(self.total),
            self.namespace,
        )
        metrics.add_read_time_per_thread(
            self.namespace, str(self.replica_id), self.total
        )
        metrics.add_totals()
        return (
            metrics.attribute_metrics,
            metrics.errors,
            metrics.errors_attr,
            metrics.attribute_read_count,
            metrics.attribute_count,
            metrics.spectrum_attribute_count,
            metrics.image_attribute_count,
            metrics.not_managed,
            metrics.namespaces_read_time,
            metrics.thread_read_time,
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i", "--replica_id", help="[Mandatory] Replica ID identification"
    )
    parser.add_argument("-r", "--replicas", help="[Mandatory] Replica count")
    parser.add_argument(
        "-n", "--namespace", help="[Mandatory] Namespace to scrape"
    )
    parser.add_argument(
        "-t",
        "--tango_host",
        help="[Mandatory] Tango host to refer to (related to the namespace)",
    )
    parser.add_argument("-l", "--log", help="log level")
    args = parser.parse_args()

    collector = NamespaceCollector()
    collector.replica_id = int(args.replica_id)
    collector.replicas = int(args.replicas)
    collector.namespace = args.namespace.strip()
    collector.tango_host = args.tango_host.strip()

    if args.log:
        level = logging.getLevelName(args.log.strip())
        logging.basicConfig(level=level)

    start_http_server(8000)
    REGISTRY.register(collector)
    while True:
        time.sleep(1)
