import threading

from prometheus_client.core import GaugeMetricFamily
from tango import CmdArgType as ArgType


class Metrics:
    def __init__(self, prefix=""):
        self.lock = threading.Lock()
        self.attribute_metrics = GaugeMetricFamily(
            prefix + "device_attribute",
            "Device attribute value",
            labels=[
                "namespace",
                "device",
                "name",
                "label",
                "str_value",
                "type",
                "dim_x",
                "dim_y",
                "x",
                "y",
            ],
        )
        self.errors = GaugeMetricFamily(
            prefix + "error_count",
            "Total number of errors reading the devices",
            labels=["namespace"],
        )
        self.errors_attr = GaugeMetricFamily(
            prefix + "error_attr_count",
            "Total number of errors reading the device attributes",
            labels=["namespace"],
        )
        self.attribute_read_count = GaugeMetricFamily(
            prefix + "attribute_read_count",
            "Total number of read attributes",
            labels=["namespace"],
        )
        self.attribute_count = GaugeMetricFamily(
            prefix + "attribute_count",
            "Total number of attributes",
            labels=["namespace"],
        )
        self.spectrum_attribute_count = GaugeMetricFamily(
            prefix + "spectrum_attribute_count",
            "Total number of spectrum attributes",
            labels=["namespace"],
        )
        self.image_attribute_count = GaugeMetricFamily(
            prefix + "image_attribute_count",
            "Total number of image attributes",
            labels=["namespace"],
        )
        self.not_managed = GaugeMetricFamily(
            prefix + "not_managed_attribute_count",
            "Total number of not managed attributes",
            labels=["namespace"],
        )
        self.namespaces_read_time = GaugeMetricFamily(
            prefix + "read_namespaces_time",
            "Read time for all namespaces in seconds",
        )
        self.thread_read_time = GaugeMetricFamily(
            prefix + "read_thread_time",
            "Read time per thread in seconds",
            labels=["namespace", "replicaid"],
        )
        self.error_count = {}
        self.error_attr_count = {}
        self.total_count = {}
        self.read_count = {}
        self.spectrum_count = {}
        self.image_count = {}
        self.not_managed_attribute_count = {}

    def add_read_time_per_thread(self, namespace, replicaid, time):
        with self.lock:
            self.thread_read_time.add_metric([namespace, replicaid], time)

    def add_read_time_namespaces(self, time):
        with self.lock:
            self.namespaces_read_time.add_metric([], time)

    def add_totals(self):
        for namespace in self.error_count:
            self.errors.add_metric(
                [namespace],
                self.error_count[namespace],
            )
        for namespace in self.error_attr_count:
            self.errors_attr.add_metric(
                [namespace],
                self.error_attr_count[namespace],
            )
        for namespace in self.total_count:
            self.attribute_count.add_metric(
                [namespace],
                self.total_count[namespace],
            )
        for namespace in self.read_count:
            self.attribute_read_count.add_metric(
                [namespace],
                self.read_count[namespace],
            )
        for namespace in self.spectrum_count:
            self.spectrum_attribute_count.add_metric(
                [namespace],
                self.spectrum_count[namespace],
            )
        for namespace in self.image_count:
            self.image_attribute_count.add_metric(
                [namespace],
                self.image_count[namespace],
            )
        for namespace in self.not_managed_attribute_count:
            self.not_managed.add_metric(
                [namespace],
                self.not_managed_attribute_count[namespace],
            )

    def sum_totals(
        self,
        namespace,
        perror_count,
        perror_attr_count,
        ptotal_count,
        pscalar_count,
        pspectrum_count,
        pimage_count,
        pnot_managed_attribute_count,
    ):
        with self.lock:
            if namespace not in self.error_count:
                self.error_count[namespace] = perror_count
            else:
                self.error_count[namespace] += perror_count

            if namespace not in self.error_attr_count:
                self.error_attr_count[namespace] = perror_attr_count
            else:
                self.error_attr_count[namespace] += perror_attr_count

            if namespace not in self.total_count:
                self.total_count[namespace] = ptotal_count
            else:
                self.total_count[namespace] += ptotal_count

            if namespace not in self.read_count:
                self.read_count[namespace] = pscalar_count
            else:
                self.read_count[namespace] += pscalar_count

            if namespace not in self.spectrum_count:
                self.spectrum_count[namespace] = pspectrum_count
            else:
                self.spectrum_count[namespace] += pspectrum_count

            if namespace not in self.image_count:
                self.image_count[namespace] = pimage_count
            else:
                self.image_count[namespace] += pimage_count

            if namespace not in self.not_managed_attribute_count:
                self.not_managed_attribute_count[
                    namespace
                ] = pnot_managed_attribute_count
            else:
                self.not_managed_attribute_count[
                    namespace
                ] += pnot_managed_attribute_count

    def add_to_metric(self, namespace, dev, attr_info, attr_value=None):
        if (
            attr_info.data_type == ArgType.DevShort
            or attr_info.data_type == ArgType.DevLong
            or attr_info.data_type == ArgType.DevUShort
            or attr_info.data_type == ArgType.DevULong
            or attr_info.data_type == ArgType.DevLong64
            or attr_info.data_type == ArgType.DevULong64
            or attr_info.data_type == ArgType.DevFloat
            or attr_info.data_type == ArgType.DevDouble
        ):
            if attr_value is None:
                attr_value = dev.read_attribute(attr_info.name)
            with self.lock:
                self.attribute_metrics.add_metric(
                    [
                        namespace,
                        dev.dev_name(),
                        attr_info.name,
                        attr_info.label,
                        "",
                        "float",
                        str(attr_value.dim_x),
                        str(attr_value.dim_y),
                        "0",
                        "0",
                    ],
                    float(attr_value.value),
                )
                return 1
        elif attr_info.data_type == ArgType.DevBoolean:
            if attr_value is None:
                attr_value = dev.read_attribute(attr_info.name)
            with self.lock:
                self.attribute_metrics.add_metric(
                    [
                        namespace,
                        dev.dev_name(),
                        attr_info.name,
                        attr_info.label,
                        "",
                        "bool",
                        str(attr_value.dim_x),
                        str(attr_value.dim_y),
                        "0",
                        "0",
                    ],
                    int(attr_value.value),
                )
                return 1
        elif attr_info.data_type == ArgType.DevString:
            if attr_value is None:
                attr_value = dev.read_attribute(attr_info.name)
            with self.lock:
                self.attribute_metrics.add_metric(
                    [
                        namespace,
                        dev.dev_name(),
                        attr_info.name,
                        attr_info.label,
                        str(attr_value.value),
                        "string",
                        str(attr_value.dim_x),
                        str(attr_value.dim_y),
                        "0",
                        "0",
                    ],
                    1,
                )
                return 1
        elif attr_info.data_type == ArgType.DevEnum:
            attr_config = dev.get_attribute_config(attr_info.name)
            if attr_value is None:
                attr_value = dev.read_attribute(attr_info.name)
            with self.lock:
                self.attribute_metrics.add_metric(
                    [
                        namespace,
                        dev.dev_name(),
                        attr_info.name,
                        attr_info.label,
                        str(attr_config.enum_labels[attr_value.value]),
                        "enum",
                        str(attr_value.dim_x),
                        str(attr_value.dim_y),
                        "0",
                        "0",
                    ],
                    int(attr_value.value),
                )
                return 1
        elif attr_info.data_type == ArgType.DevState:
            if attr_value is None:
                attr_value = dev.read_attribute(attr_info.name)
            with self.lock:
                self.attribute_metrics.add_metric(
                    [
                        namespace,
                        dev.dev_name(),
                        attr_info.name,
                        attr_info.label,
                        str(attr_value.value),
                        "state",
                        str(attr_value.dim_x),
                        str(attr_value.dim_y),
                        "0",
                        "0",
                    ],
                    int(attr_value.value),
                )
                return 1
        else:
            return 0

    def add_to_metric_spectrum(
        self, namespace, dev, attr_info, attr_value=None
    ):
        if attr_value is None:
            attr_value = dev.read_attribute(attr_info.name)
        for x in range(int(attr_value.dim_x)):
            with self.lock:
                if (
                    attr_info.data_type == ArgType.DevShort
                    or attr_info.data_type == ArgType.DevLong
                    or attr_info.data_type == ArgType.DevUShort
                    or attr_info.data_type == ArgType.DevULong
                    or attr_info.data_type == ArgType.DevLong64
                    or attr_info.data_type == ArgType.DevULong64
                    or attr_info.data_type == ArgType.DevFloat
                    or attr_info.data_type == ArgType.DevDouble
                ):
                    self.attribute_metrics.add_metric(
                        [
                            namespace,
                            dev.dev_name(),
                            attr_info.name,
                            attr_info.label,
                            "",
                            "float",
                            str(attr_value.dim_x),
                            str(attr_value.dim_y),
                            str(x),
                            "0",
                        ],
                        float(attr_value.value[x]),
                    )
                elif attr_info.data_type == ArgType.DevBoolean:
                    self.attribute_metrics.add_metric(
                        [
                            namespace,
                            dev.dev_name(),
                            attr_info.name,
                            attr_info.label,
                            "",
                            "bool",
                            str(attr_value.dim_x),
                            str(attr_value.dim_y),
                            str(x),
                            "0",
                        ],
                        int(attr_value.value[x]),
                    )
                elif attr_info.data_type == ArgType.DevString:
                    self.attribute_metrics.add_metric(
                        [
                            namespace,
                            dev.dev_name(),
                            attr_info.name,
                            attr_info.label,
                            str(attr_value.value[x]),
                            "string",
                            str(attr_value.dim_x),
                            str(attr_value.dim_y),
                            str(x),
                            "0",
                        ],
                        1,
                    )
                elif attr_info.data_type == ArgType.DevEnum:
                    self.attribute_metrics.add_metric(
                        [
                            namespace,
                            dev.dev_name(),
                            attr_info.name,
                            attr_info.label,
                            str(attr_value.value[x]),
                            "enum",
                            str(attr_value.dim_x),
                            str(attr_value.dim_y),
                            str(x),
                            "0",
                        ],
                        int(attr_value.value[x]),
                    )
                elif attr_info.data_type == ArgType.DevState:
                    self.attribute_metrics.add_metric(
                        [
                            namespace,
                            dev.dev_name(),
                            attr_info.name,
                            attr_info.label,
                            str(attr_value.value[x]),
                            "state",
                            str(attr_value.dim_x),
                            str(attr_value.dim_y),
                            str(x),
                            "0",
                        ],
                        int(attr_value.value[x]),
                    )
                else:
                    pass
        return 1

    def add_to_metric_image(self, namespace, dev, attr_info):
        attr_value = dev.read_attribute(attr_info.name)
        for y in range(int(attr_value.dim_y)):
            for x in range(int(attr_value.dim_x)):
                with self.lock:
                    if (
                        attr_info.data_type == ArgType.DevShort
                        or attr_info.data_type == ArgType.DevLong
                        or attr_info.data_type == ArgType.DevUShort
                        or attr_info.data_type == ArgType.DevULong
                        or attr_info.data_type == ArgType.DevLong64
                        or attr_info.data_type == ArgType.DevULong64
                        or attr_info.data_type == ArgType.DevFloat
                        or attr_info.data_type == ArgType.DevDouble
                    ):
                        self.attribute_metrics.add_metric(
                            [
                                namespace,
                                dev.dev_name(),
                                attr_info.name,
                                attr_info.label,
                                "",
                                "float",
                                str(attr_value.dim_x),
                                str(attr_value.dim_y),
                                str(x),
                                str(y),
                            ],
                            float(attr_value.value[y][x]),
                        )
                    elif attr_info.data_type == ArgType.DevBoolean:
                        self.attribute_metrics.add_metric(
                            [
                                namespace,
                                dev.dev_name(),
                                attr_info.name,
                                attr_info.label,
                                "",
                                "bool",
                                str(attr_value.dim_x),
                                str(attr_value.dim_y),
                                str(x),
                                str(y),
                            ],
                            int(attr_value.value[y][x]),
                        )
                    elif attr_info.data_type == ArgType.DevString:
                        self.attribute_metrics.add_metric(
                            [
                                namespace,
                                dev.dev_name(),
                                attr_info.name,
                                attr_info.label,
                                str(attr_value.value[y][x]),
                                "string",
                                str(attr_value.dim_x),
                                str(attr_value.dim_y),
                                str(x),
                                str(y),
                            ],
                            1,
                        )
                    elif attr_info.data_type == ArgType.DevEnum:
                        self.attribute_metrics.add_metric(
                            [
                                namespace,
                                dev.dev_name(),
                                attr_info.name,
                                attr_info.label,
                                str(attr_value.value[y][x]),
                                "enum",
                                str(attr_value.dim_x),
                                str(attr_value.dim_y),
                                str(x),
                                str(y),
                            ],
                            int(attr_value.value[y][x]),
                        )
                    elif attr_info.data_type == ArgType.DevState:
                        self.attribute_metrics.add_metric(
                            [
                                namespace,
                                dev.dev_name(),
                                attr_info.name,
                                attr_info.label,
                                str(attr_value.value[y][x]),
                                "state",
                                str(attr_value.dim_x),
                                str(attr_value.dim_y),
                                str(x),
                                str(y),
                            ],
                            int(attr_value.value[y][x]),
                        )
                    else:
                        pass
        return 1
