import os

databaseds_name = "sys/database/2"
cluster_domain = os.getenv("CLUSTER_DOMAIN")


def build_fqdn(tango_host, namespace, dev_name):
    print(tango_host)
    print(namespace)
    print(dev_name)
    return (
        "tango://"
        + tango_host
        + "."
        + namespace
        + ".svc."
        + cluster_domain
        + ":10000/"
        + dev_name
    )
