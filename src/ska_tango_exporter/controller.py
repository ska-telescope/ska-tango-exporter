# Deprecated
import argparse
import logging
import os
import time

import requests
import yaml
from kubernetes import client, config

from ska_tango_exporter.k8s import K8sHelper


# return namespace-tango_host name dicationary
def get_active_namespaces(list_service_for_all_namespaces):
    res_active_namespaces = {}
    for item in list_service_for_all_namespaces:
        exp_namespaces = os.getenv("EXP_NAMESPACES")
        if exp_namespaces and item.metadata.namespace not in exp_namespaces:
            continue

        if "databaseds" not in item.metadata.name:
            continue

        if item.metadata.namespace not in res_active_namespaces:
            res_active_namespaces[item.metadata.namespace] = item.metadata.name

    return res_active_namespaces


# return namespace-svcs name dicationary
def get_configured_namespaces(list_service_for_all_namespaces):
    res_configured_namespaces = {}
    for item in list_service_for_all_namespaces:
        # if not item.metadata.namespace.strip() == controller_namespace.strip():
        #     continue

        if "exporter" in item.metadata.name:
            continue

        if "prometheus" in item.metadata.name:
            continue

        if "svc" not in item.metadata.name:
            continue

        configured_namespace = item.metadata.name.split("-svc-")[0]

        if configured_namespace not in res_configured_namespaces:
            res_configured_namespaces[configured_namespace] = []

        res_configured_namespaces[configured_namespace].append(
            item.metadata.name
        )

    return res_configured_namespaces


def configure_exporter_namespace(
    databaseds,
    target_namespace,
    controller_namespace,
    preplicas,
    pregistry,
    pimage,
    ptag,
    ppull_policy,
):
    configured_exporters = []
    logging.info(
        "Configuring exporter(s) for %s on namespace %s",
        databaseds,
        target_namespace,
    )

    for replica_number in range(preplicas):

        stateful_set = k8s_helper.stateful_manifest(
            target_namespace + "-sts-" + str(replica_number),
            target_namespace + "-svc-" + str(replica_number),
            controller_namespace,
            pregistry,
            pimage,
            ptag,
            ppull_policy,
            databaseds,
            target_namespace,
            replica_number,
            preplicas,
        )
        svc = k8s_helper.svc_manifest(
            target_namespace + "-svc-" + str(replica_number),
            controller_namespace,
        )

        try:
            logging.info(
                "creating statefulset %s for namespace %s",
                str(replica_number),
                target_namespace,
            )
            apps_api.create_namespaced_stateful_set(
                namespace=namespace, body=stateful_set
            )
            logging.info(
                "created statefulset %s for namespace %s",
                str(replica_number),
                target_namespace,
            )
            logging.info(
                "creating service %s for namespace %s",
                str(replica_number),
                target_namespace,
            )
            core_v1_api.create_namespaced_service(
                namespace=namespace, body=svc
            )
            logging.info(
                "created service %s for namespace %s",
                str(replica_number),
                target_namespace,
            )
        except Exception as ex:
            logging.error(ex)

        configured_exporters.append(
            target_namespace + "-svc-" + str(replica_number)
        )

    logging.info(
        "Configured exporter(s) for %s on namespace %s",
        databaseds,
        target_namespace,
    )
    return configured_exporters


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--namespace", help="Namespace of the exporter")
    parser.add_argument("-r", "--replicas", help="Pods number per namespace")
    parser.add_argument("-g", "--registry", help="Image registry for docker")
    parser.add_argument("-i", "--image", help="Docker image of the collector")
    parser.add_argument("-t", "--tag", help="Docker tag of the collector")
    parser.add_argument(
        "-e",
        "--evaluation_interval",
        default="5s",
        help="Prometheus evaluation timeout",
    )
    parser.add_argument(
        "-s",
        "--scrape_interval",
        default="5s",
        help="Prometheus scrape interval",
    )
    parser.add_argument(
        "-c",
        "--scrape_timeout",
        default="5s",
        help="Prometheus scrape timeout",
    )
    parser.add_argument(
        "-p", "--pull_policy", help="Docker pull policy of the collector"
    )
    parser.add_argument(
        "-m", "--prometheus", help="Prometheus url for reload configuration"
    )
    parser.add_argument("-l", "--log", help="log level")
    args = parser.parse_args()

    if args.log:
        level = logging.getLevelName(args.log.strip())
        logging.basicConfig(level=level)

    namespace = args.namespace.strip()
    registry = args.registry.strip()
    image = args.image.strip()
    tag = args.tag.strip()
    pull_policy = args.pull_policy.strip()

    config.load_incluster_config()
    core_v1_api = client.CoreV1Api()
    apps_api = client.AppsV1Api()
    k8s_helper = K8sHelper()
    replicas = int(args.replicas)

    logging.info("Configuring dummy prometheus")
    prometheus_config = {
        "global": {
            "evaluation_interval": args.evaluation_interval,
            "scrape_interval": args.scrape_interval,
            "scrape_timeout": args.scrape_timeout,
        },
        "scrape_configs": [],
    }
    with open(r"/etc/prometheus/prometheus.yml", "w", encoding="utf8") as file:
        yaml.dump(prometheus_config, file)

    logging.info("Configured dummy prometheus")

    while True:
        update = False
        targets = []

        # search for databaseds services in all namespace
        result = core_v1_api.list_service_for_all_namespaces()
        active_namespaces = get_active_namespaces(result.items)

        # search for configured exporter services in the exporter namespace
        result = core_v1_api.list_namespaced_service(namespace)
        configured_namespaces = get_configured_namespaces(result.items)

        # configuring
        for n in active_namespaces:
            if n in configured_namespaces:
                for target in configured_namespaces[n]:
                    targets.append(target)
                continue

            result = configure_exporter_namespace(
                active_namespaces[n],
                n,
                namespace,
                replicas,
                registry,
                image,
                tag,
                pull_policy,
            )
            for res in result:
                targets.append(res)
            update = True

        # cleaning
        for n in configured_namespaces:
            if n in active_namespaces:
                continue

            for i in range(replicas):
                logging.info(
                    "removing service (not active anymore) %s from namespace %s.",
                    str(i),
                    n,
                )
                apps_api.delete_namespaced_stateful_set(
                    name=n + "-sts-" + str(i), namespace=namespace
                )
                core_v1_api.delete_namespaced_service(
                    name=n + "-svc-" + str(i), namespace=namespace
                )
                logging.info(
                    "removed service (not active anymore) %s from namespace %s.",
                    str(i),
                    n,
                )

            update = True

        # update prometheus
        if update and len(targets) > 0:
            logging.info("Configuring prometheus for the namespaces available")
            prometheus_config = {
                "global": {
                    "evaluation_interval": args.evaluation_interval,
                    "scrape_interval": args.scrape_interval,
                    "scrape_timeout": args.scrape_timeout,
                },
                "scrape_configs": [
                    {
                        "job_name": "tango",
                        "static_configs": [{"targets": targets}],
                    }
                ],
            }
            with open(
                r"/etc/prometheus/prometheus.yml", "w", encoding="utf8"
            ) as file:
                yaml.dump(prometheus_config, file)

            logging.info("Configured prometheus for the namespaces available")

            try:
                requests.post(args.prometheus.strip(), timeout=5)
            except Exception as e2:
                logging.error(e2)

        time.sleep(1)
