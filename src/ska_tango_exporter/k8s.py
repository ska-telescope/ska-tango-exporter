# Deprecated
import logging
import os

cluster_domain = os.getenv("CLUSTER_DOMAIN")


class K8sHelper(object):
    def __init__(self):
        pass

    def svc_manifest(self, name, namespace):
        return {
            "apiVersion": "v1",
            "kind": "Service",
            "metadata": {"name": f"{name}", "namespace": f"{namespace}"},
            "spec": {
                "selector": {"app": f"{name}"},
                "type": "NodePort",
                "ports": [{"protocol": "TCP", "port": 80, "targetPort": 8000}],
            },
        }

    def stateful_manifest(
        self,
        name,
        svc_name,
        namespace,
        registry,
        image,
        tag,
        pull_policy,
        tango_host,
        namespace_ref,
        id_replica,
        replicas,
    ):
        return {
            "apiVersion": "apps/v1",
            "kind": "StatefulSet",
            "metadata": {
                "name": f"{name}",
                "namespace": f"{namespace}",
                "labels": {"app": f"{svc_name}"},
            },
            "spec": {
                "serviceName": f"{svc_name}",
                "replicas": 1,
                "selector": {"matchLabels": {"app": f"{svc_name}"}},
                "template": {
                    "metadata": {"labels": {"app": f"{svc_name}"}},
                    "spec": {
                        "initContainers": [
                            {
                                "name": "wait-databaseds",
                                "image": f"{registry}/{image}:{tag}",
                                "imagePullPolicy": f"{pull_policy}",
                                "command": [
                                    "/usr/local/bin/wait-for-it.sh",
                                    f"{tango_host}.{namespace_ref}.svc.{cluster_domain}:10000",  # noqa: E501
                                    "--timeout=30",
                                    "--strict",
                                    "--",
                                    "echo ready.",
                                ],
                            }
                        ],
                        "containers": [
                            {
                                "name": "ska-tango-exporter",
                                "image": f"{registry}/{image}:{tag}",
                                "imagePullPolicy": f"{pull_policy}",
                                "ports": [{"containerPort": 8000}],
                                "env": [
                                    {
                                        "name": "TANGO_HOST",
                                        "value": f"{tango_host}.{namespace_ref}.svc.{cluster_domain}:10000",  # noqa: E501
                                    }
                                ],
                                "command": ["python"],
                                "args": [
                                    "/app/src/ska_tango_exporter/namespace_collector.py",  # noqa: E501
                                    f"-i {id_replica}",
                                    f"-r {replicas}",
                                    f"-n {namespace_ref}",
                                    f"-t {tango_host}",
                                    f"-l {logging.getLevelName(logging.getLogger().getEffectiveLevel())}",
                                ],
                                "livenessProbe": {
                                    "failureThreshold": 5,
                                    "httpGet": {
                                        "path": "/metrics",
                                        "port": 8000,
                                    },
                                    "initialDelaySeconds": 120,
                                    "periodSeconds": 30,
                                    "successThreshold": 1,
                                    "timeoutSeconds": 5,
                                },
                                "resources": {
                                    "requests": {
                                        "cpu": "100m",
                                        "memory": "128Mi",
                                        "ephemeral-storage": "128Mi",
                                    },
                                    "limits": {
                                        "cpu": "1000m",
                                        "memory": "256Mi",
                                        "ephemeral-storage": "256Mi",
                                    },
                                },
                            }
                        ],
                    },
                },
            },
        }
