# device health
# add push gateway
# subscribe to all events and push to the gateway
# state dashboard

import argparse
import logging
import os
import threading
import time

import tango
from influxdb_client import InfluxDBClient
from influxdb_client.client.write_api import SYNCHRONOUS
from tango import Database

from ska_tango_exporter.dev_factory import DevFactory
from ska_tango_exporter.utils import build_fqdn


class EventsCollector(object):
    def __init__(self, pinflux_url):
        self.tango_host = None
        self.namespace = None
        self.replicas = None
        self.replica_id = None

        self.influx_url = pinflux_url
        self.influx_bucket_name = os.environ.get(
            "INFLUXDB_BUCKET_NAME", "tango-events"
        )
        # self.influx_collection_name = os.environ.get("INFLUXDB_ORG", "events")
        # self.influx_db_user = os.environ.get("INFLUX_DB_USER", "root")
        self.influx_db_token = os.environ.get("INFLUX_DB_TOKEN")
        self.influx_db_org = os.environ.get("INFLUX_DB_ORG")
        self.evt_subscribed = {}
        self._dev_factory = DevFactory()

    def handle_event(self, evt):
        """
        evt: EventData
            device : (DeviceProxy) The DeviceProxy object on which the call was executed.
            attr_name : (str) The attribute name
            event : (str) The event name
            attr_value : (DeviceAttribute) The attribute data (DeviceAttribute)
            err : (bool) A boolean flag set to true if the request failed. False otherwise
            errors : (sequence<DevError>) The error stack
            reception_date: (TimeVal)
        """
        dev_name = evt.device.name()
        attr_name_simple = self.get_attr_name_simple(evt)
        logging.debug("event arrived from %s/%s", dev_name, attr_name_simple)
        self.log_event(evt)

    def main_thread(self):
        while True:
            collector.subscribe_to_events()
            time.sleep(1)

    def log_event(self, evt):
        """
        Take an event, build a json dictionary and store in DB.
        evt: EventData
            device : (DeviceProxy) The DeviceProxy object on which the call was executed.
            attr_name : (str) The attribute name
            event : (str) The event name
            attr_value : (DeviceAttribute) The attribute data (DeviceAttribute)
            err : (bool) A boolean flag set to true if the request failed. False otherwise
            errors : (sequence<DevError>) The error stack
                DevError
                    reason : (str) reason
                    severity : (ErrSeverity) error severty (WARN, ERR, PANIC)
                    desc : (str) error description
                    origin : (str) Tango server method in which the error happened
            reception_date: (TimeVal)
        """
        # build the json event to store in db
        attr_name_simple = self.get_attr_name_simple(evt)
        attr_info = evt.device.attribute_query(attr_name_simple)
        errors = {}
        if evt.err:
            for error in evt.errors:
                errors += {
                    "reason": error.reason,
                    "severity": error.severity,
                    "desc": error.desc,
                    "origin": error.origin,
                }
            logging.error(errors)
        json_data_evt = {
            "device": evt.device.dev_name(),
            "namespace": self.namespace,
            "attr_name": attr_info.name,
            "event": evt.event,
            "error": evt.err,
            "reception_date": evt.reception_date,
            "attr_value": evt.attr_value.value,
        }

        self.push_to_influxdb(json_data_evt)

    def push_to_influxdb(self, json_data_evt):
        try:
            with InfluxDBClient(
                url=self.influx_url,
                token=self.influx_db_token,
                org=self.influx_db_org,
            ) as client:
                write_api = client.write_api(write_options=SYNCHRONOUS)
                write_api.write(
                    bucket=self.influx_bucket_name,
                    org=self.influx_db_org,
                    record=json_data_evt,
                    record_measurement_key="device",
                    record_time_key=json_data_evt["reception_date"],
                    record_tag_keys=[
                        "namespace",
                        "attr_name",
                        "event",
                    ],
                    record_field_keys=["attr_value", "error"],
                )
        except Exception as e:
            logging.error("InfluxDBClient error: %s", str(e), stack_info=True)

    def get_attr_name_simple(self, evt):
        attr_name_split = str(evt.attr_name).split("/")
        attr_name_simple = attr_name_split[len(attr_name_split) - 1]
        if "." in attr_name_simple:
            attr_name_simple = attr_name_simple.split(".")[0]
        return attr_name_simple

    def subscribe_to_events(self):
        server_list = []
        try:
            database = Database()
            server_list = database.get_server_list().value_string
        except tango.DevFailed as df:
            logging.error(
                "Databaseds connection error: %s",
                str(df.args[0].reason),
            )
            return

        # logging.debug("server list= %s", str(len(server_list)))
        count = int(len(server_list) / self.replicas)
        i = int(count * self.replica_id)
        count += i
        if self.replicas - 1 == self.replica_id:
            count = len(server_list)

        while i < count and count <= len(server_list):
            class_list = database.get_device_class_list(
                str(server_list[i])
            ).value_string
            j = 0
            while j < len(class_list):
                dev_name = class_list[j]
                if "dserver" in dev_name:
                    j += 2
                    continue
                if "/" not in dev_name:
                    j += 2
                    continue
                if "sys/access_control/1" in dev_name:
                    j += 2
                    continue
                if "sys/rest/0" in dev_name:
                    j += 2
                    continue

                try:
                    dev = self._dev_factory.get_device(
                        build_fqdn(self.tango_host, self.namespace, dev_name)
                    )
                except tango.DevFailed as df:
                    logging.error(
                        "_dev_factory.get_device %s error: %s",
                        dev_name,
                        str(df.args[0].reason),
                    )
                    continue

                try:
                    attr_list = dev.attribute_list_query_ex()
                except tango.DevFailed as df:
                    logging.error(
                        "dev.attribute_list_query_ex %s error: %s",
                        dev_name,
                        str(df.args[0].reason),
                    )
                    continue

                for attr_info in attr_list:
                    # ms = dev.get_attribute_poll_period(attr_info.name)
                    if (
                        dev_name + attr_info.name
                    ) not in self.evt_subscribed or (
                        (dev_name + attr_info.name) in self.evt_subscribed
                        and self.evt_subscribed[dev_name + attr_info.name] < 3
                    ):
                        if (
                            dev_name + attr_info.name
                        ) not in self.evt_subscribed:
                            self.evt_subscribed[dev_name + attr_info.name] = 0
                        else:
                            self.evt_subscribed[dev_name + attr_info.name] += 1
                        try:
                            self.evt_subscribed[
                                dev_name + attr_info.name
                            ] = dev.subscribe_event(
                                attr_info.name,
                                tango.EventType.CHANGE_EVENT,
                                self.handle_event,
                            )
                        except tango.DevFailed as df:
                            logging.debug(
                                "%s/%s subscribe_event failed (try #%s): %s",
                                dev_name,
                                attr_info.name,
                                str(
                                    self.evt_subscribed[
                                        dev_name + attr_info.name
                                    ]
                                ),
                                str(df.args[0].reason),
                            )
                j += 2
            i += 1

        logging.debug("subscribe_to_events..done")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i", "--replica_id", help="[Mandatory] Replica ID identification"
    )
    parser.add_argument("-r", "--replicas", help="[Mandatory] Replica count")
    parser.add_argument(
        "-n", "--namespace", help="[Mandatory] Namespace to scrape"
    )
    parser.add_argument(
        "-t",
        "--tango_host",
        help="[Mandatory] Tango host to refer to (related to the namespace)",
    )
    parser.add_argument(
        "-d",
        "--influxdb",
        help="[Mandatory] InfluxDB connection url",
    )
    parser.add_argument("-l", "--log", help="log level")
    args = parser.parse_args()
    collector = EventsCollector(args.influxdb.strip())
    collector.replica_id = int(args.replica_id)
    collector.replicas = int(args.replicas)
    collector.namespace = args.namespace.strip()
    collector.tango_host = args.tango_host.strip()

    if args.log:
        level = logging.getLevelName(args.log.strip())
        logging.basicConfig(level=level)
    thread0 = threading.Thread(target=collector.main_thread, daemon=True)
    thread0.start()
    while True:
        time.sleep(1)
