#
PROJECT = ska-tango-exporter

# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm. If this does not already exist it will be created
KUBE_NAMESPACE ?= ska-tango-exporter

# HELM_RELEASE is the release that all Kubernetes resources will be labelled with
HELM_RELEASE ?= exporter0

HELM_CHART ?= ska-tango-exporter
HELM_CHARTS_TO_PUBLISH ?= ska-tango-exporter

K8S_UMBRELLA_CHART_PATH ?= charts/test-exporter/
K8S_CHARTS ?= ska-tango-exporter test-exporter

CI_PROJECT_DIR ?= .

MINIKUBE ?= false ## Minikube or not

SKA_TANGO_OPERATOR ?= true

EXPORTER_PASSWORD ?= exporter

TANGO_HOST ?= databaseds:10000

CLUSTER_DOMAIN ?= cluster.local

PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=./src:/app/src:/app/src/ska_tango_exporter KUBE_NAMESPACE=$(KUBE_NAMESPACE) HELM_RELEASE=$(HELM_RELEASE) CLUSTER_DOMAIN=$(CLUSTER_DOMAIN) TANGO_HOST=$(TANGO_HOST)

PYTHON_SWITCHES_FOR_FLAKE8=--ignore=F401,W503 --max-line-length=180

PYTHON_VARS_AFTER_PYTEST = -m 'not post_deployment' --disable-pytest-warnings

GRAFANA_EXTRA_DATA_SOURCE ?=

REPLICAS ?= 1#number of services to create for scraping

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#

# include OCI Images support
include .make/oci.mk

# include k8s support
include .make/k8s.mk

# include Helm Chart support
include .make/helm.mk

# Include Python support
include .make/python.mk

# include raw support
include .make/raw.mk

# include core make support
include .make/base.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

ifeq ($(strip $(firstword $(MAKECMDGOALS))),k8s-test)
# need to set the PYTHONPATH since the ska-cicd-makefile default definition 
# of it is not OK for the alpine images
PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=./src:/app/src:/app/src/ska_tango_exporter:/usr/local/lib/python3.9/site-packages TANGO_HOST=$(TANGO_HOST) KUBE_NAMESPACE=$(KUBE_NAMESPACE) HELM_RELEASE=$(HELM_RELEASE) CLUSTER_DOMAIN=$(CLUSTER_DOMAIN) TANGO_HOST=$(TANGO_HOST)
endif

ifneq ($(CI_REGISTRY),)
K8S_IMAGE_PARAMS = --set ska-tango-exporter.tango_exporter.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
	--set ska-tango-exporter.tango_exporter.image.registry=$(CI_REGISTRY)/ska-telescope/ska-tango-exporter
K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/ska-tango-exporter/ska-tango-exporter:$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
else
K8S_IMAGE_PARAMS = --set ska-tango-exporter.tango_exporter.image.tag=$(VERSION) \
	--set ska-tango-exporter.tango_exporter.image.registry=$(CAR_OCI_REGISTRY_HOST)
K8S_TEST_IMAGE_TO_TEST = $(CAR_OCI_REGISTRY_HOST)/ska-tango-exporter:$(VERSION)
endif

K8S_CHART_EXTRA?=
ifeq ($(MINIKUBE),true)
K8S_CHART_EXTRA += --set ska-tango-exporter.prometheus.resources.requests.memory=256Mi \
					 --set ska-tango-exporter.prometheus.resources.limits.memory=512Mi \
					 --set ska-tango-exporter.grafana.resources.requests.memory=256Mi \
					 --set ska-tango-exporter.grafana.resources.limits.memory=512Mi
endif

ifneq ($(GRAFANA_EXTRA_DATA_SOURCE),)
K8S_CHART_EXTRA += --values $(GRAFANA_EXTRA_DATA_SOURCE)
endif

K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
	--set global.tango_host=$(TANGO_HOST) \
	--set global.cluster_domain=$(CLUSTER_DOMAIN) \
	--set global.operator=$(SKA_TANGO_OPERATOR) \
	--set ska-tango-exporter.replicas=$(REPLICAS) \
	--set ska-tango-exporter.mongodb.auth.passwords=$(EXPORTER_PASSWORD) \
	$(K8S_IMAGE_PARAMS) \
	$(K8S_CHART_EXTRA)

k8s-test: PYTHON_VARS_AFTER_PYTEST := --disable-pytest-warnings 