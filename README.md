# ska-tango-exporter

A prometheus exporter for TANGO system(s) deployed in k8s namespaces

## Design
The python module ``controller.py`` is the main entry of the project. It checks, in the k8s-cluster where it is deployed, the namespaces expressed in the parameter ``tango_exporter.exp_namespaces`` of the helm chart values file: if the namespace contains a TANGO databaseds device then it creates a number of k8s services (module ``namespace_collector.py``, parameter ``replicas``) that will read every attributes of every TANGO devices exported in the database. 

The helm chart deploys also a prometheus service configured for reading all the information from the exported created. 

The prometheus instance can be then added as data source in a Grafana web application.

## Installation

This project is structured to use k8s for development and testing so that the build environment, test environment and test results are all completely reproducible and are independent of host environment. It uses ``make`` to provide a consistent UI (run ``make help`` for targets documentation).

### Install docker

Follow the instructions available at [here](https://docs.docker.com/get-docker/).

### Install minikube

You will need to install `minikube` or equivalent k8s installation in order to set up your test environment. You can follow the instruction at [here](https://gitlab.com/ska-telescope/sdi/deploy-minikube/):
```
git clone git@gitlab.com:ska-telescope/sdi/deploy-minikube.git
cd deploy-minikube
make all
eval $(minikube docker-env)
```

*Please note that the command `eval $(minikube docker-env)` will point your local docker client at the docker-in-docker for minikube. Use this only for building the docker image and another shell for other work.*

## How to Use

Install the application with: 

```
git clone https://gitlab.com/ska-telescope/ska-tango-exporter.git
cd ska-tango-exporter
make k8s-install-chart
```

Test the application with:

```
make k8s-test
```

Uninstall the application with: 
```
make k8s-uninstall-chart
```

Build a new Docker image for the project:
```
$ make oci-build
[...]
[+] Building 111.7s (14/14) FINISHED 
[...]
```

Python linting:
```
$ make python-lint
[...]
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
```

Helm Charts linting:
```
$ make helm-lint
[...]
10 chart(s) linted, 0 chart(s) failed
```



python3 -c "import tango; dev=tango.DeviceProxy('test/tabata/1');dev.start()"