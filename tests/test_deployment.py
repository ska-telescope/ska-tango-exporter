import json
import os
from unittest.mock import Mock

import pytest
import requests
from tango import DeviceProxy

import ska_tango_exporter.controller
from ska_tango_exporter.controller import (
    configure_exporter_namespace,
    get_active_namespaces,
    get_configured_namespaces,
)
from ska_tango_exporter.k8s import K8sHelper
from ska_tango_exporter.model import Metrics
from ska_tango_exporter.namespace_collector import NamespaceCollector

device_name = "sys/database/2"
exp_namespaces = namespace = os.getenv("KUBE_NAMESPACE")


@pytest.mark.post_deployment
def test_get():
    kube_namespace = os.getenv("KUBE_NAMESPACE")
    url = f"http://prometheus-{os.getenv('HELM_RELEASE')}:9090/{kube_namespace}/prometheus"

    response = requests.get(url, timeout=5)
    assert response.status_code == 200


@pytest.mark.post_deployment
def test_collector():
    collector = NamespaceCollector()
    collector.replica_id = int(0)
    collector.replicas = int(1)
    collector.namespace = namespace
    collector.tango_host = os.getenv("TANGO_HOST")
    (
        _,
        errors,
        errors_attr,
        attribute_read_count,
        attribute_count,
        spectrum_attribute_count,
        image_attribute_count,
        not_managed,
        _,
        _,
    ) = collector.collect()

    assert errors.samples[0].value > 1
    assert errors_attr.samples[0].value > 1
    assert attribute_read_count.samples[0].value > 10
    assert image_attribute_count.samples[0].value > 10
    assert attribute_count.samples[0].value > 10
    assert spectrum_attribute_count.samples[0].value > 10
    assert not_managed.samples[0].value > 10


@pytest.mark.post_deployment
def test_metrics():
    metrics = Metrics()

    dev = DeviceProxy(device_name)

    res = metrics.add_to_metric(namespace, dev, dev.attribute_query("State"))
    assert res == 1
    assert metrics.attribute_metrics.samples[0].labels["str_value"] == "ON"


@pytest.mark.skip(reason="deprecated")
def test_K8sHelper():
    k8s_helper = K8sHelper()
    sts_name = "sts_name"
    srv_name = "srv_name"
    replica_number = 0
    preplicas = 1

    stateful_set = k8s_helper.stateful_manifest(
        sts_name,
        srv_name,
        namespace,
        "pregistry",
        "pimage",
        "ptag",
        "ppull_policy",
        "databaseds",
        "target_namespace",
        replica_number,
        preplicas,
    )

    assert "databaseds.target_namespace.svc." + os.getenv(
        "CLUSTER_DOMAIN"
    ) + ":10000" in json.dumps(stateful_set)

    svc = k8s_helper.svc_manifest(
        srv_name,
        namespace,
    )
    assert svc["metadata"]["name"] == srv_name


@pytest.mark.skip(reason="deprecated")
def test_controller_get_active_namespaces():
    service_item = Mock()
    service_item.metadata.namespace = "ska-tango-exporter"
    service_item.metadata.name = "databaseds"
    list_service_for_all_namespaces = []
    list_service_for_all_namespaces.append(service_item)
    res_active_namespaces = get_active_namespaces(
        list_service_for_all_namespaces
    )
    assert (
        res_active_namespaces[service_item.metadata.namespace]
        == service_item.metadata.name
    )


@pytest.mark.skip(reason="deprecated")
def test_controller_get_configured_namespaces():
    service_item = Mock()
    service_item.metadata.namespace = "namespace"
    service_item.metadata.name = "namespace-svc-0"
    list_service_for_all_namespaces = []
    list_service_for_all_namespaces.append(service_item)
    res_configured_namespace = get_configured_namespaces(
        list_service_for_all_namespaces
    )
    assert res_configured_namespace[service_item.metadata.namespace] == [
        service_item.metadata.name
    ]


@pytest.mark.post_deployment
def test_controller_configure_exporter_namespace():
    ska_tango_exporter.controller.core_v1_api = Mock()
    ska_tango_exporter.controller.apps_api = Mock()
    ska_tango_exporter.controller.k8s_helper = K8sHelper()
    ska_tango_exporter.controller.namespace = namespace
    replicas = 5
    res_configured_exporters = configure_exporter_namespace(
        "databaseds",
        "target_namespace",
        "controller_namespace",
        replicas,
        "pregistry",
        "pimage",
        "ptag",
        "ppull_policy",
    )
    ska_tango_exporter.controller.apps_api.create_namespaced_stateful_set.assert_called()
    ska_tango_exporter.controller.core_v1_api.create_namespaced_service.assert_called()
    for i in range(replicas):
        assert "target_namespace-svc-" + str(i) in res_configured_exporters
