---
apiVersion: v1
kind: Service
metadata:
  name: grafana-{{ $.Release.Name }}
  namespace: {{ .Release.Namespace }}
spec:
  selector:
    subsystem: grafana
    app: ska-tango-exporter
  type: {{ $.Values.grafana.service_type}}
  ports:
  - protocol: TCP
    port: 3000
    targetPort: 3000

---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: grafana-{{ $.Release.Name }}
  namespace: {{ .Release.Namespace }}
  labels:
    subsystem: grafana
    app: ska-tango-exporter
spec:
  serviceName: grafana-{{ $.Release.Name }}
  replicas: 1
  selector:
    matchLabels:
      subsystem: grafana
      app: ska-tango-exporter
  template:
    metadata:
      labels:
        subsystem: grafana
        app: ska-tango-exporter
    spec:
      securityContext:
        runAsUser: 472
        fsGroup: 472
      containers:
      - name: grafana
        image: "{{ .Values.grafana.image.registry }}/{{ .Values.grafana.image.image }}:{{ .Values.grafana.image.tag }}"
        imagePullPolicy: {{ .Values.grafana.image.pullPolicy }}
        ports:
        - containerPort: 3000
        env:
        - name: GF_PANELS_DISABLE_SANITIZE_HTML
          value: "true"
        - name: "GF_PLUGINS_ALLOW_LOADING_UNSIGNED_PLUGINS"
          value: '{{- join "," .Values.grafana.unsigned_plugins }}'
        - name: "GF_INSTALL_PLUGINS"
          value: '{{- join "," .Values.grafana.plugins }}'
        - name: "GF_PATHS_DATA"
          value: "/var/lib/grafana"
        volumeMounts:
        - name: data
          mountPath: /var/lib/grafana
        - name: config
          mountPath: /etc/grafana/provisioning/datasources
        - name: exampledashboards
          mountPath: /etc/grafana/provisioning/dashboards/examples
        - name: exampledef
          mountPath: /etc/grafana/provisioning/dashboards/example.yml
          subPath: example.yml
        - name: exampledef
          mountPath: /etc/grafana/grafana.ini
          subPath: grafana.ini
        resources:
{{ toYaml .Values.grafana.resources | indent 10 }}
      volumes:
      - name: config
        configMap:
          name: grafana-datasource-{{ .Release.Name }}
      - name: exampledashboards
        configMap:
          name: grafana-json-boards-{{ .Release.Name }}
      - name: exampledef
        configMap:
          name: grafana-examples-{{ .Release.Name }}
      - name: data
        persistentVolumeClaim:
          claimName: grafana-data-{{ .Release.Name }}

---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: grafana-{{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
spec:
  ingressClassName: nginx
  rules:
  - http:
      paths:
      - path: /{{ .Release.Namespace }}/grafana/(.*)
        pathType: ImplementationSpecific
        backend:
          service:
            name: grafana-{{ $.Release.Name }}
            port:
              number: 3000
