---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: "prometheus-data-{{ .Release.Name }}"
  namespace: {{ .Release.Namespace }}
  labels:
    subsystem: prometheus
    app: ska-tango-exporter
spec:
{{- if .Values.global.minikube }}
  storageClassName: standard
{{- else }}
  storageClassName: nfss1
{{- end }}
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 10Gi

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: "prometheus-config-{{ .Release.Name }}"
  namespace: "{{ .Release.Namespace }}"
  labels:
    subsystem: prometheus
    app: ska-tango-exporter
data:
  prometheus.yml: |
    global:
      scrape_interval: {{ .Values.prometheus.scrape_interval }}
      evaluation_interval: {{ .Values.prometheus.evaluation_interval }}
      scrape_timeout: {{ .Values.prometheus.evaluation_interval }}
    scrape_configs:
{{- if .Values.global.operator }}
      - job_name: operator
        metrics_path: /ska-tango-operator/metrics
        static_configs: 
          - targets:
            - "{{ .Values.prometheus.tango_operator_metrics }}"
{{- end }}
      - job_name: tango
        static_configs: 
          - targets:
{{- range $i, $e := until (.Values.replicas | int ) }}
            - "collector-{{ $.Release.Name }}-{{ $i }}"
{{- end }}
{{- if .Values.prometheus.k8s_api_server.enabled }}
      - job_name: 'cadvisor'
        kubernetes_sd_configs:
        - role: node
          api_server: "{{ .Values.prometheus.k8s_api_server.server }}"
          # TLS config required in two places
          tls_config:
            ca_file: /etc/prometheus/cert/ca.crt
          bearer_token_file: /etc/prometheus/cert/token
        scheme: https
        # TLS config required in two places
        tls_config:
          insecure_skip_verify: true
        bearer_token_file: /etc/prometheus/cert/token
        metrics_path: /metrics/cadvisor
{{- end }}

{{- if .Values.prometheus.k8s_api_server.enabled }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: prometheus-k8s-ca-{{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
data: 
  ca.crt: |-
{{ .Values.prometheus.k8s_api_server.ca | indent 4 }}
  token: |-
    {{ .Values.prometheus.k8s_api_server.token }}
{{- end }}
